# Monitools laravel tracker client

# install

add this code to your composer.json
  
``` bash
"repositories":[
      {
          "type": "vcs",
          "url" : "git@bitbucket.org:olenin_denis/tracker_client.git"
      }
  ]
```

add to require:

``` bash
"monitools/client": "dev-master"
```
run

``` bash
composer update
```

### config dsn

```dotenv
MONITOOLS_LARAVEL_DSN="Enter dsn from your profile"
```
