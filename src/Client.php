<?php

namespace Monitools\TrackingLaravel;

use Exception;
use GuzzleHttp\Client as HttpClient;

class Client
{
    /**
     * @var Formatter
     */
    private $formatter;

    /**
     * Client constructor.
     * @param Formatter $formatter
     * @param $options
     */
    public function __construct(Formatter $formatter, $options)
    {
        $this->formatter = $formatter;
    }

    /**
     * @param Exception $exception
     * @return mixed|\Psr\Http\Message\ResponseInterface
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function captureException(Exception $exception)
    {
        $response = null;
        $time_start = microtime(true);

        $content = $this->formatter->handle($exception);

        $dsn = config('tracking.dsn');
        $client = new HttpClient([
            'timeout' => 4,
            'connect_timeout' => 5
        ]);
        try {
            $response = $client->post($dsn, [
                'headers' => [
                    'Accept' => 'application/json',
                ],
                'json' => $content,
                'debug' => true
            ]);
        } catch (Exception $e) {
            dump($e->getMessage());
        }

        $time_end = microtime(true);
        $execution_time = ($time_end - $time_start)/60;
        dump($execution_time);

        return $response;
    }
}