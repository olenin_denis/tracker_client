<?php

namespace Monitools\TrackingLaravel;

use Exception;

class Formatter
{
    static private $codeLines = 10;
    static private $codeLength = 20;

    /** @var Exception null  */
    private $exception = null;

    public function handle(Exception $exception)
    {
        $result = [];

        $this->exception = $exception;

        $result['type'] = get_class($exception);
        $result['message'] = $this->exception->getMessage();
        $result['code'] = $this->exception->getCode();
        $result['file'] = $this->exception->getFile();
        $result['line'] = $this->exception->getLine();
        $result['request'] = $this->getRequest();
        $result['user'] = $this->getUser();
        $result['details'] = $this->getDetails();
        $result['trace'] = $this->getTrace();

        return $result;
    }

    private function getUser()
    {
        $user = request()->user();

        if (!$user) {
            return null;
        }

        return [
            'id' => $user->id
        ];
    }

    private function getRequest()
    {
//        dd(request()->route());
        $request = request();

        return [
            'ip' => $request->ip(),
            'headers' => $request->headers,
            'method' => $request->getMethod(),
            'realMethod' => $request->getRealMethod(),
            'idempotent' => $request->isMethodIdempotent(),
            'pathInfo' => $request->getPathInfo(),
            'basePath' => $request->getBasePath(),
            'baseUrl' => $request->getBaseUrl(),
            'scheme' => $request->getScheme(),
            'port' => $request->getPort(),
            'httpHost' => $request->getHttpHost(),
            'requestUri' => $request->getRequestUri(),
            'schemaWithHost' => $request->getSchemeAndHttpHost(),
            'uri' => $request->getUri(),
            'queryString' => $request->getQueryString(),
            'secure' => $request->isSecure(),
            'host' => $request->getHost(),
            'format' => $request->getRequestFormat(),
            'contentType' => $request->getContentType(),
            'defaultLocale' => $request->getDefaultLocale(),
            'locale' => $request->getLocale(),
            'protocolVersion' => $request->getProtocolVersion(),
            'eTags' => $request->getETags(),
            'charsets' => $request->getCharsets(),
            'encodings' => $request->getEncodings(),
            'xmlHttpRequest' => $request->isXmlHttpRequest(),
        ];
    }

    private function getContent($file, $line)
    {
        $content = file_get_contents($file);
        $results = [];

        if (isset($content)) {
            $lines = explode("\n", $content);

            if (($line) !== null) {
                $line = ($line);
                $start = (int)($line - self::$codeLines);
                $length = self::$codeLength;
                if ($start < 0) {
                    $start = 0;
                }

                $lines = array_slice($lines, $start, $length, true);
                foreach ($lines as $key => $code) {
                    $lineNum = $key+1;

                    $errorLine = ($line == $lineNum) ? true : false;
                    array_push($results, [
                        "line" => "{$lineNum}. {$code}",
                        "error" => $errorLine
                    ]);
                }
            }
        }

        return $results;
    }

    private function getDetails()
    {
        $lines = null;
        $errorLine = $this->exception->getLine();
        $file = $this->exception->getFile();

        return $this->getContent($file, $errorLine);
    }

    private function getTrace()
    {
        $trace = $this->exception->getTrace();
        $results = [];

        foreach ($trace as $item) {
            if (array_key_exists('file', $item) and array_key_exists('line', $item)) {
                $content = $this->getContent($item["file"], $item["line"]);
                array_push($results, [
                    "file" => $item["file"],
                    "line" => $item["line"],
                    "content" => $content,
                ]);
            } else {
                array_push($results, [
                    "class" => $item["class"],
                    "type" => $item["type"],
                    "function" => $item["function"],
                ]);
            }
        }

        return $results;
    }
}