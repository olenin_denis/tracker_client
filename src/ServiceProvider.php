<?php

namespace Monitools\TrackingLaravel;

use Illuminate\Support\ServiceProvider as IlluminateServiceProvider;

class ServiceProvider extends IlluminateServiceProvider
{
    /**
     * Type to bind tracking as in the Service Container.
     *
     * @var string
     */
    public static $abstract = 'tracking';

    /**
     * Boot the service provider.
     */
    public function boot()
    {
        $this->publishes([
            __DIR__.'/config/tracking.php' => config_path('tracking.php'),
        ]);
//        $this->app->make(self::$abstract);

//        if ($this->hasDsnSet()) {
//            $this->bindEvents($this->app);
//        }

//        if ($this->app->runningInConsole()) {
//            $this->commands([
//                TestCommand::class,
//            ]);
//        }
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->mergeConfigFrom(
            __DIR__.'/config/tracking.php', 'tracking'
        );

        $this->app->bind(static::$abstract, function ($app) {
            return new Client(new Formatter(), $app);
        });

//        $this->app->singleton(static::$abstract, function () {
//            $basePath = base_path();
//            $options = [
//                'environment' => $this->app->environment(),
//                'prefixes' => [$basePath],
//                'project_root' => $basePath,
//                'in_app_exclude' => [$basePath . '/vendor'],
//            ];
//
//            return new Client($options);
//        });
    }

    /**
     * Check if a DSN was set in the config.
     *
     * @return bool
     */
    protected function hasDsnSet(): bool
    {
        $config = $this->app['config'][static::$abstract];

        return !empty($config['dsn']);
    }
}
