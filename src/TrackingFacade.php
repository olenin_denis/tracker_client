<?php

namespace Monitools\TrackingLaravel;

use Illuminate\Support\Facades\Facade;

class TrackingFacade extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'tracking';
    }
}